import os

from rucio.client.uploadclient import Client, UploadClient
from common.rucio.helpers import createCollection

from tasks.task import Task
from utility import generateRandomFile



class BasicTest(Task):
    """ Rucio basic test uploading a file. """

    def __init__(self, logger):
        super().__init__(logger)
        self.rse = None
        self.scope = None
        self.lifetime = None
        self.size = None
        self.namingPrefix = None

    def run(self, args, kwargs):
        super().run()

        self.tic()
        try:
            self.rse = kwargs["rse"]
            self.scope = kwargs["scope"]
            self.lifetime = kwargs["lifetime"]
            self.size = kwargs["size"]
            self.namingPrefix = kwargs.get("naming_prefix", "")
        except KeyError as e:
            self.logger.critical("Could not find necessary kwarg for test.")
            self.logger.critical(repr(e))
            return False

        # Create a dataset to house the data, named with today's date
        # and scope <scope>.
        #
        datasetDID = createCollection(self.logger.name, self.scope)

        # Upload a file of size from <sizes> to thr RSE
        #
        # Generate random file of size <size>
        f = generateRandomFile(self.size, prefix=self.namingPrefix)
        fileDID = "{}:{}".format(self.scope, os.path.basename(f.name))

        try:
            items = [{
                "path": f.name,
                "rse": self.rse,
                "did_scope": self.scope,
                "lifetime": self.lifetime,
                "register_after_upload": True,
                "force_scheme": None,
                "transfer_timeout": 60,
            }]
            client = UploadClient(logger=self.logger)
            client.upload(items=items)

            self.logger.debug("Upload complete")

            # Attach to dataset
            try:
                client = Client(logger=self.logger)
                tokens = datasetDID.split(":")
                toScope = tokens[0]
                toName = tokens[1]
                attachment = {"scope": toScope, "name": toName, "dids": []}
                for did in fileDID.split(" "):
                    tokens = fileDID.split(":")
                    scope = tokens[0]
                    name = tokens[1]
                    attachment["dids"].append({"scope": scope, "name": name})
                client.attach_dids_to_dids(attachments=[attachment])
                self.logger.debug("Attached file to dataset")
            except Exception as e:
                self.logger.warning(repr(e))

        except Exception as e:
            self.logger.warning("Upload failed: {}".format(e))
        os.remove(f.name)

        self.toc()
        self.logger.info("Finished in {}s".format(round(self.elapsed)))
