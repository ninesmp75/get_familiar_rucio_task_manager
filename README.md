# Rucio basic test uploading a randon file to a rse

# Architecture

```
  ├── Dockerfile
  ├── etc
  │   └── tasks
  │      └── proofs
  │         └── tests
  │           └── basic_test.yml
  ├── LICENSE
  ├── Makefile
  ├── README.md
  ├── requirements.txt
  └── src
      |── common
      └── tasks
         └── tests
            └── basic_test.py

```


# Usage

This framework is designed to be run in a dockerised environment. Images should be built off a preexisting dockerised Rucio client image. Extended images currently exist for the prototype skao datalake.

In Dockerfile, change environment variable:
```bash
ENV TASK_FILE_RELPATH etc/tasks/stubs.yml
```
Then, build image:
```bash
eng@ubuntu:~/rucio-task-manager$ sudo make skao
```

## Required environment variables

To use the framework, it is first necessary to set a few environment variables. A brief description of each is given
below:

- **RUCIO_CFG_AUTH_TYPE**: the authentication type (userpass||x509||oidc)
- **TASK_FILE_PATH**: the relative path from the package root to the task file or url

Depending on whether they are already set in the image's baked-in `rucio.cfg`, the following may need to be set:

- **RUCIO_CFG_RUCIO_HOST**: the Rucio server host
- **RUCIO_CFG_AUTH_HOST**: the Rucio auth host
- **RUCIO_CFG_ACCOUNT**: the Rucio account under which the tasks are to be performed

Additionally, there are authentication type dependent variables that must be set.

### Authentication by OpenID Connect

If the user has a valid access token:

- **OIDC_ACCESS_TOKEN**: an encrypted oidc-agent client with refresh token

This token can be retrieved by authenticating with Rucio using the client and taking the resulting token from
`/tmp/user/.rucio_user/auth_token_for_account_<account>`. This method is advised for general development use.

Both methods require the Rucio account to be explicitly set:

- **RUCIO_CFG_ACCOUNT**: the Rucio account under which the tasks are to be performed

Depending on whether they are already set in the image's baked-in `rucio.cfg`, the following may need to be set:

- **RUCIO_CFG_OIDC_SCOPE**: list of OIDC scopes
- **RUCIO_CFG_OIDC_AUDIENCE**: list of OIDC audiences

## Example

### With oidc (by passing in an access token)

#### Getting an access token (using the Rucio Client)

To develop tasks that can be tested by running against an existing datalake, we must first retrieve a valid access
token for the datalake. In this example, we will use OIDC (and assume our datalake instance supports this method of
authentication).

This token can be retrieved by running a Rucio client container and authenticating against your account,
`<account>`, with OIDC, e.g. using the SKAO Rucio prototype client:

```bash
$ docker run -it --rm -e RUCIO_CFG_ACCOUNT=<account>> -e RUCIO_CFG_AUTH_TYPE=oidc registry.gitlab.com/ska-telescope/src/ska-rucio-client:release-1.29.0
$ rucio whoami
```

Please use your internet browser, go to:

    https://rucio.srcdev.skao.int/auth/oidc_redirect?BLABLABLA....

and authenticate with your Identity Provider.

Copy the provide token and export it to an environment variable, `OIDC_ACCESS_TOKEN`, in whatever shell you intend to run the
manager in. It's advisable to keep the Rucio client container open as you will need it to generate new tokens when one
expires.

#### Using this access token to run tasks in a rucio-task-manager container

In a **new shell**, run:

```bash
eng@ubuntu:~/rucio-task-manager$ docker run --rm -it \
-e RUCIO_CFG_AUTH_TYPE=oidc \
-e RUCIO_CFG_ACCOUNT=<account> \
-e RUCIO_CFG_AUTH_HOST=https://rucio.srcdev.skao.int \
-e RUCIO_CFG_RUCIO_HOST=https://rucio.srcdev.skao.int \
-e OIDC_ACCESS_TOKEN="$OIDC_ACCESS_TOKEN" \
-e TASK_FILE_PATH=etc/tasks/tests/basic_test.yml \
-v $RUCIO_TASK_MANAGER_ROOT:../rucio-task-manager \
--name=rucio-task-manager rucio-task-manager:`cat BASE_RUCIO_CLIENT_TAG`
```

to run the task at `etc/tasks/tests/basic_test.yml`. Note that in this example, the task manager package is volume mounted in to
the container so any changes you have locally will be reflected at runtime.

# Results
![Basic Test](figures/basic_test.png)


